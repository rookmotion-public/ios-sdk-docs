
# RookMotion iOS SDK

RookMotion iOS SDK allows the implementation of the RookMotion functionalities for training measurement, tracking, and analysis.

These functionalities are:

* User's info, trainings, rewards, and statistics management in complement with the RookMotion API.
* Bluetooth management for training sensors.

# Keywords

| keyword      | Def |
| ----------- | ----------- |
| **Client**  | Billing account that contracts the use of the API, a client can have multiple centers, branches, rooms or be virtual  |
| **client_key**  | Unique identifier provided by the RookMotion API account manager for each center/branch/room of the client  |
| **User**  | A person which uses the RookMotion API through the client APP to perform trainings. |
| **user_uuid**  | Unique identifier for each user, it's generated by the RookMotion API for each user added with API.addUser.|
| **Sensor**  | A Bluetooth peripheral device with a training variable measurement such as heart rate.    |
| **sensor_uuid**  | Unique identifier for each training sensor, it's generated by the RookMotion API for each sensor added to the user with API.addSensor.|



# Bluetooth Package

###  Scanner.class

```swift
public class Scanner
```

Manages the discovering heart rate sensors nearby to the iOS device and available for connection. The developer is able to call the methods sequentially start/get/stop discovery to have more control of the discovery or use the method find_sensors which automates the process.

Note: Sensors are filtered to only those with the  **(180D)** Bluetooth service of heart rate measurement

| Returns      | Function |
| ----------- | ----------- |
| success or error      | ```startDiscovery()``` |
| success or error      | ```registerCallback(completion: (([String]) -> Void)))``` |
| ```[CBPeripheral]```  | ```getSensorsDiscovered() ```        |
|success or error | ```stopDiscovery()``` |
|```[CBPeripheral]``` or error | ```discoverSensors(timeout: Int, completion: ((Int) -> Void))?)``` |


### startDiscovery()
```swift
public func startDiscovery() throws
```
Starts the sensors discovering, this is performed repeatedly until the stop method is called. Each time a sensor is found it's appended to a list that can be obtained with the getSensorsFound method. With registerCallback method, a function can be registered as a handler to be called each time a sensor is appended to the list.

This clears the list of sensors found in the previous discovery.

Note: Sensors are filtered to only those with the  **(180D)** Bluetooth service of heart rate measurement

Throws **permission errors, errors when activating sensors, errors when reading properties of a sensor like a sensor's name**. 
##### Example 
```swift
// Discover sensors
let scanner  = Scanner()

do{
	scanner.startDiscovery()
}catch let error {
	print(error)
}

```

### registerCallback()
```swift
public func registerCallback(completion: (([String]) -> Void)))
```
Registers a reference to a callback function, which is called during the discovery each time a sensor is found and appended to the list.

##### Example 
```swift
// Discover sensors
let scanner  = Scanner()

// Create a handler
func callback(sensorList: [String]) {
	print(sensorList.description)
}

scanner.registerCallback(completion: callback(sensorList:))
```

### getSensorsDiscovered()
```swift
public func getSensorsDiscovered() throws -> [CBPeripheral]
```
Retrieves the list of heart rate sensors discovered nearby to the iOS device and available for connection.

##### Example 
```swift
let scanner  = Scanner()

do{
	let currentSensorList =	scanner.getSensorsDiscovered()
	print(currentSensorList.description)
}catch let error {
	print(error)
}

```

### stopDiscovery()
```swift
public func stopDiscovery() throws
```
Stops the sensors discovering.

##### Example 
```swift
let scanner  = Scanner()

do{
	scanner.startDiscovery()
		// Some process
	scanner.stopDiscovery()
}catch let error {
	print(error)
}

```

### discoverSensors()
```swift
public func discoverSensors(timeout: Int, completion: (([String]) -> Void))?) throws -> [CBPeripheral]
```
Automates the start/get/stop sensors discovery. 
1. If a callback function is provided it's registered with registerCallback.

2. Starts the sensors discovering with startDiscovery.

3. Waits (timeout) seconds to discover sensors.

	3.1. If a callback function is provided, it'll be called once per sensor found during the waiting time.

4. Stops the discovering after timeout.

5. Returns the list of sensors found

##### Example with callback
```swift
let scanner  = Scanner()
let time = 10

// Create a callback
func callback(sensorList: [CBPeripheral]) {
	print(sensorList.description)
}

do{
	let sensorsDiscovered = scanner.discoverSensors(timeout: time, 
	completion: callback(sensorList:) )
}catch let error {
	print(error)
}

```

##### Example without callback
```swift
let scanner  = Scanner()
let time = 10

do{
	let sensorsDiscovered = 
	scanner.discoverSensors(seconds:time,completion:nil)
	print(sensorsDiscovered.description)
	
}catch let error {
	print(error)
}

```


###  Sensor.class

```swift
public class Sensor
```
The Sensor class is used to manage the sensor Bluetooth connection (actions and status), as well as notifications (actions and acquisition). This class creates an unconnected sensor, the connect method should be called to specify and connect with the sensor.

| Returns      | Function |
| ----------- | ----------- |
| success or error      | ```connect(device: CBPeripheral)``` |
| ```Int```  | ```connectionStatus() ```        |
|success or error | ```enableNotifications(completion: ((hr: Int, steps: Int) -> Void))?)``` |
|```Ìnt``` | ```getHeartRate()``` |
|```Ìnt``` | ```getSteps()``` |
|```Bool``` | ```measuresSteps()``` |
|success or error | ```disconnect(device: CBPeripheral)``` |


### connect()
```swift
public func connect(device: CBPeripheral) throws
```
Makes a connection to the sensor given as ```CBPeripheral``` discovered by the Scanner.
Throws **connectionError**, otherwise the connection will be stablished.

##### Example 
```swift
// Connect a sensor
let sensor  = Sensor()
let sensor = CBPeripheral

do{
	sensor.connect(device: sensor)
}catch let error {
	print(error)
}
```

### connectionStatus()
```swift
public func connectionStatus()
```
Retrieves the sensor Bluetooth connection status.

Status list:
**case 0**
`case disconnected`

* The sensor is disconnected.

**case 1**
`case connecting`

* The sensor is connecting.

**case 2**
`case connected`

* The sensor is connected.

**case 3**
`case disconnecting`

* The sensor is disconnecting.
##### Example 
```swift
// Connection status of the sensor
let sensor  = Sensor()

let sensorStatus = sensor.connectionStatus()
print("connection status \(sensorStatus)")
```


### enableNotifications()
```swift
public func enableNotifications(completion: (([String:Any]) -> Void))?) throws
```

Enables heart rate notifications. For RookMotion arm sensors with steps measurement, the steps notification will be also enabled.

If a callback function is provided: Registers a reference to a callback function, which is called each time the sensor notifies a new measurement.

##### Example 
```swift
let sensor = Sensor()

func notificationsCallback(dict: [String:Any]) {
	print("new values \(dict.description)")
}

sensor.enableNotifications( notificationsCallback(dict:))
```

### getHeartRate()
```swift
public func getHeartRate() -> Int
```
Retrieves the last Heart Rate measurement received.

##### Example 
```swift
let sensor  = Sensor()

let hr = sensor.getHeartRate()
print("Hear Rate at this momment \(hr) ")
```

### getSteps()
```swift
public func getSteps() -> Int
```
Retrieves the last steps count received.

##### Example 
```swift
let sensor  = Sensor()

let steps =	sensor.getSteps()
print("Steps at this momment \(steps) ")
```

### measuresSteps()
```swift
public func canMeasuresSteps() throws -> Bool 
```
Detects if the connected sensor has steps measurement compatibility.

##### Example 
```swift
let sensor  = Sensor()

do{
	let measuresSteps = sensor.measuresSteps()
	if measuresSteps {
		....//code
	}else {
		//code
	}
}catch let error {
	print(error)
}

```

### disconnect()
```swift
public func disconnect(device: CBPeripheral) throws
```

Drops the connection with the sensor.

Throws **disconnectionError**, otherwise the connection will be dropped.

##### Example 
```swift

// Connect a sensor
let sensor  = Sensor()
let sensor = CBPeripheral

do{
	sensor.disconnect(device: sensor)
}catch let error {
	print(error)
}

```



# Training Package

###  Training.class

```swift
public class Training
```
Provides automated methods to perform trainings using Bluetooth sensors handling all the involved steps, storage, and processing of the training data, and upload to server.

| Returns      | Function |
| ----------- | ----------- |
| success or error | ```start(with: CBPeripheral , completion: (([String: Any]) -> Void))?)``` |
|success or error| ```stop(completion: ((summary: UIView?) -> Void))?) ```        |


### start() 
> **Under construction**

```swift
public func start(with: CBPeripheral , completion: (([String: Any]) -> Void))?) throws
```

Automates the Bluetooth methods for sensor discovery, selection, connection, re-connection, notification enabling, and data recording. While the stop method is not called, this process will keep the sensor connected and notifying, in case of disconnection a reconnection process will be performed.

If a callback function is not provided: A default design view will be displayed and updated each time new data is generated for the training view

Else if a callback function is provided: Registers a reference to a callback function, which is called each time new data is generated for the training view.

Training data includes training duration, heart rate, steps, calories, effort...  If the sensor has no steps measurement, the steps will be 0.

Throws errors for **Connection, notifications, discovery** and others related to sensor connection, also errors for data conversion.

##### Example 
```swift
let training  = Training()
// This sensor was declared just for example purposes
let sensor : CBPeripheral?

func callback(dict: [String: Any]){
	// code
}

do{
	training.start(with: sensor, callback(dict:))
}catch let error {
	print(error)
}
```

### stop() 
> **Under construction**

```swift
public func stop(completion: ((summary: UIView?) -> Void))?) throws -> [String,Any]
```
Automates the Bluetooth methods for sensor disconnection, training data summary, and upload. 

If a callback function is not provided: A default design view will be displayed with the training records plotted and summaries displayed.

Else if a callback function is provided: Registers a reference to a callback function, which is called when the training data is processed, the developer will be responsible for generating his own training summary view.

Training data include heart rate, calories, effort, and steps records. Summaries include (min, avg, and max) values for the given records.

Returns a dictionary with the training data.

If the sensor has no steps measurement, the steps and summaries won't be passed.

In the background, uploads the current training as well as any other trainings pending to be uploaded to the RookMotion Server.

##### Example with a callback
```swift
let training  = Training()

func callback(summary: UIView?){
	// code
	myView.add(summary)
}

do{
	_ = training.stop(callback(summary:))
}catch let error {
	print(error)
}
```

##### Example without callback
```swift
let training  = Training()

do{
	let trainingData = training.stop(nil)
	print("this is the training data \(trainingData.description)")
}catch let error {
	print(error)
}
```



# Cloud Package

###  Network.class

```swift
public class Network
```

Provides methods for network or internet actions



| Returns      | Function |
| ----------- | ----------- |
| ```Bool```  | ```internetIsAvailable()``` |
| ```Bool```  | ```apiIsAlive() ```  |


### internetIsAvailable()
```swift
public func internetIsAvailable() -> Bool
```
This function is used to know if internet connection is available (mobile or wifi).
##### Example 
```swift
let network  = Network()

let internetIsAvailable = network.internetIsAvailable()

if internetIsAvailable {
	//code
}
```

###  apiIsAlive()
```swift
public func apiIsAlive() -> Bool
```

This function is used to know if the RookMotion server is available this may help you to know if the developer can send or receive data and establish communication with the server.

##### Example 
```swift
let network  = Network()

let serverIsAlive = network.apiIsAlive()

if serverIsAlive {
	//code
}
```


###  API.class

```swift
public class Api
```

Provides methods to interact with the RookMotion server. Involves CRUD actions for users, sensors and trainings.


| Returns      | Function |
| ----------- | ----------- |
| ```user_uuid: String```  | ```addUser(email: String)``` |
| ```user_indexes: [Int]```  | ```updateUserProfile(name: String, pseudonym: String, sex: String,restingHeartRate: Int, height: Double, weight: Double, birthday: String)```  |
| ```sensor_uuid: Int``` or **error** | ```addSensor(sensorName: String)``` |
| ```sensors: [String]```  | ```getSensorsList()``` |
| success or error  | ```uploadPendingData()``` |
| HTTP Code  | ```addTraining(training: TrainingObj)``` |
| ```[JSON]```  | ```getTrainingsList(fromDate: String, toDate: String)``` |
| ```JSON: String```  | ```addReward(dict: [String:Int])``` |
|```[JSON]```| ```getRewardsList(fromDate: String, toDate: String)```|
| ```[String]```| ```getTrainingTypesList(id: String)```|
| HTTP Code| ```setMembershipID(id: String)```|


### addUser()
```swift
public func addUser(email: String) -> String
```

Creates a new user on the RookMotion server for the email provided.
Returns a RookMotion user_uuid used for all the subsequent requests.

##### Example 
```swift
let api  = Api()
let myUserEmail = "user@email.com"

let uuid = api.addUser(email: myUserEmail)
print("Your rookmotion uuid is \(uuid)")
```


### updateUserProfile()
```swift
public func updateUserProfile(name: String, pseudonym: String, sex: String,restingHeartRate: Int, height: Double, weight: Double, birthday: String) -> [Int]
```
Updates the user's information and physiological variables.

##### Example 
```swift
let api  = Api()

let indexes = api.updateUserProfile(name: "name" , 
			pseudonym: "pseudonym", 
			sex:"male",
			restingHeartRate: 65, 
			height: 190.0, 
			weight: 90.0,
			birthday: "1990-01-01")
		
print("User updated)")
```


### addSensor()
```swift
public func addSensor(sensorName: String) -> Int
```
Adds a new user's sensor on the RookMotion server, it's created with the sensorName.
Returns a RookMotion sensor_uuid used for all the subsequent requests.

##### Example 
```swift
let api  = Api()
let sensor = Sensor()
sensor.name = "RoockC2-028476"

let sensor_uuid = api.addSensor(sensorName: sensor.name)
print("Your sensor id is \(sensor_uuid)")
```

### getSensorsList()
```swift
public func getSensorsList() -> [String]
```

Retrieves a list of user's sensors.
##### Example 
```swift
let api  = Api()

let sensors = api.getSensorsList()
for item in sensors {
	print("sensors \(item.description)")
}
```

### uploadPendingData()
```swift
public func uploadPendingData() throws
```
Gathers from the internal storage and uploads to the RookMotion server the data pending to be uploaded such as training, sensors, and profile changes. After a successful upload, the internal storage is cleaned. 

##### Example 
```swift
let api  = Api()

do {
	api.uploadPendingData()
} catch let error {
	print(error)
}
```

### addTraining()
```swift
public func addTraining(training: TrainingObj) -> String
```

Adds a user's training on the RookMotion server including (records, summaries, and rewards).
##### Example 
```swift
let api  = Api()

let training = TrainingObj()

let httpResponse = api.addTraining(training: training)
print("http code \(httpResponse)")
```


### getTrainingsList()
```swift
public func getTrainingsList(fromDate: String, toDate: String) -> [JSON]
```
Retrieves a list of user's trainings during the specified period. The list includes only some summaries of the training to display the list. Records are not included, these can be gotten with getTrainingInfo

##### Example 
```swift
let api  = Api()
let from = "2020-10-10"
let to = "2020-10-20"


let trainings = api.getTrainingsList(fromDate: from,toDate: to)
for item in trainings {
	print("trainings \(item.description)")
}
```

### getTrainingTypesList()
```swift
public func getTrainingTypesList() -> [String]
```
Retrieves a list of available training types for the client's account. 

##### Example 
```swift
let api  = API()

let trainingsTypes = api.getTrainingTypesList()
for item in trainingsTypes {
	print("trainingsTypes \(item.description)")
}
```

### addReward()
```swift
public func addReward(dict: [String:Int]) -> String
```

Adds a user's reward on the RookMotion server.
##### Example 
```swift
let api  = Api()

parameters = [
	"reward_type": 1
	"value": 250
] as [String,Any]

let response = api.addReward(dict: parameters)
```


### getRewardsList()
```swift
public func getRewardsList(fromDate: String, toDate: String) -> [JSON]
```
Retrieves a list of user's rewards during the specified period. 

##### Example 
```swift
let api  = API()
let from = "2020-10-10"
let to = "2020-10-20"


let rewards = api.getRewardsList(fromDate: from,toDate: to)
for item in rewards {
	print("reward \(item.description)")
}
```


### setMembershipID()
```swift
public func setMembershipID(id: String)
```
Sets the user's membership id which identifies the user on the client's database.

##### Example 
```swift
let api  = API()
let myMembershipID = "sldkfhs78dfb03"

let httpStatus = api.setMembershipID(id: myMembershipID)
print("Response http \(httpStatus)")
```


# Storage Package

###  Storage.class

```swift
public class Storage
```

This class can be used for the developer and for other functions in RM package,  the main purpose of this class is to store information to be consulted later on many situations.

| Returns      | Function |
| ----------- | ----------- |
| ```Bool```  | ```storeUserInfo(user: User)``` |
| ```String```  | ```readUserUuid()```  |
| ```[Sensor]```  | ```readSensorsList()```  |
| ```Bool```  | ```storeTraining(training: TrainingObj)```  |
| ```Bool```  | ```storeSensor(sensorName: String)```  |
| ``Int``| ```countTrainingsNotUploaded()```|
| ``Bool``| ```storeTrainingTypesList(list: [String])```|
| ``[String]``| ```readTrainingTypesList()```|
| success or error| ```storeReward(dict: [String:Any])```|
| ```User```| ```readUserInfo()```|
| success or error| ```storeUserUUID(uuid: String)```|




### storeUserInfo()
```swift
public func storeUserInfo(user: User) -> Bool
```
Stores the user information in the internal storage. This is required for RookMotion calculations and compensations.

##### Example 
```swift

// Save Usr info on storage
let storageInstance = Storage()
let userToStore = User()

userToStore.name = "James"
userToStore.sex = "male"

let stored = storageInstance.storeUserInfo(user: userToStore)
print("Stored \(stored)")

```

### readUserUuid()
```swift
public func readUserUuid() -> String
```

Retrieves user_uuid stored locally.
##### Example 

```swift

// Save user_uuid
let storageInstance = Storage()
let userUUID = storageInstance.readUserUuid()

let parameters  = [
	"user_uuid": userUUID
] as [String : Any]

```

### readSensorsList()
```swift
public func readSensorsList() -> [Sensor]
```

Retrieves the user's sensor list stored locally.

##### Example 
```swift

// Get stored user´s sensor list
let storageInstance = Storage()
let sensorList = storageInstance.readSensorsList()

for sensor in sensorList {
	print("Sensor name \(sensor.name)")
}
```

### storeTraining()
```swift
public func storeTraining(training: TrainingObj) -> Bool
```

Stores temporarily the user's training until it's uploaded to the server. Trainings are not kept locally permanently in order to save storage space.

##### Example 
```swift

// Save training info
let storageInstance = Storage()
let training = TrainingObj()

training.graphs = realTimeTrainingGraphs //its just an example of values

let stored = storageInstance.storeSensor(training: training)
if stored {
	.... //code
}

```

### storeSensor()
```swift
public func storeSensor(sensorName: String) -> Bool
```
Stores the user's sensor in the local storage.
##### Example 
```swift

// Save sensor info
let storageInstance = Storage()
let sensorName = "RookC2-097834"

let stored = storageInstance.storeSensor(sensorName: sensorName)
if stored {
	.... //code
}

```

### countTrainingsNotUploaded()
```swift
public func countTrainingsNotUploaded() -> Int
```
Retrieves a count of trainings stored locally and pending to be uploaded and removed from the internal storage.

##### Example 
```swift

// Get the number of saved trainings
let storageInstance = Storage()
let numberOfTrainings = storageInstance.countTrainingsNotUploaded()

for i in 0..<numberOfTrainings {
	...//code
}

```

### storeTrainingTypesList()
```swift
public func storeTrainingTypesList(list: [String]) -> Bool
```
Stores the training types list available for the client account in the local storage.

##### Example 
```swift
let storageInstance = Storage()
let api  = API()

let trainingsTypes = api.getTrainingTypesList()

let stored = storageInstance.storeTrainingTypesList(list: trainingsTypes)
```

### readTrainingTypesList()
```swift
public func readTrainingTypesList() -> [String]
```

Retrieves the training types list available for the client account stored locally.

##### Example 
```swift
let storageInstance = Storage()

let list = storageInstance.readTrainingTypesList()
for item in list {
	print("training_type \(item.description)")
}
```


### storeReward()
```swift
public func storeReward(dict: [String:Any])
```
Stores a reward that for some reason couldn't be uploaded before and will be sent to the web service when there is an internet connection available.
##### Example 
```swift
let storageInstance = Storage()

parameters = [
	"reward_type": 1
	"value": 250
] as [String:Any]

let user = storageInstance.storeReward(dict: parameters)
```

### readUserInfo()
```swift
public func readuserInfo() -> User?
```
Retrieves a user object that contains all the user info stored on the database. 
This object can be nil if there is no info stored.
##### Example 
```swift
let storageInstance = Storage()

let user = storageInstance.readUserInfo()
print("Stored user name \(user?.name)")
```


### storeUserUUID()
```swift
public func storeUserUUID(uuid: String)
```
Stores the userUUID provided by RookMotion in the shared preferences.

##### Example 
```swift
let storageInstance = Storage()
let myUUID = "ñlasdhf74bSFgne80f4F3"

storageInstance.storeUserUUID(uuid: myUUID)
```






## RookMotion

This documentation is provided by **RookMotion** all rights reserved to Rookeries Development SRL de CV.

**contact** : contacto@rookmotion.com  

Startup México SUM Campus Ciudad de México  

+52 55 39406461